@component('admin.components.image_preview',['imageName'=>$model->avatar])
	@slot('show')
	  @if(!empty($model->avatar))
	  	true
	  @endif
	@endslot
	{{ $slot }}
@endcomponent